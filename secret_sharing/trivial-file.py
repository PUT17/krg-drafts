import random
import sys
import os.path

#
# Program use lipsum.txt file to divide each byte as a secret.
# Files secretfileX.txt contains 5 different parts. In main module,
# program is dividing lipsum.txt byte by byte and appending
# secrets to corresponding sercretpart files.
# First bytes of secretparts contains data for first byte of
# lipsum file, second for second etc.
#


def divide_byte(byte, n):
    s = int.from_bytes(byte, byteorder=sys.byteorder)
    parts = []
    k = 255
    sum_of_current_calculated_parts = 0
    for i in range(n - 1):
        si = random.randint(0, k - 1)
        sum_of_current_calculated_parts += si
        parts.append(si)
    parts.append((s - sum_of_current_calculated_parts) % k)
    return parts

if __name__ == "__main__":
    print("Lab 3")
    filename = "lipsum.txt"
    divide_mode = True
    n = 5                       # liczba udzialow
    k = 255
    divided_bytes = []

    if divide_mode:
        with open(filename, "rb") as f:
            byte = f.read(1)
            while byte != b"":
                byte_parts = divide_byte(byte, n)
                divided_bytes.append(byte_parts)
                byte = f.read(1)
            for singlebyte in divided_bytes:
                for i in range(n):
                    with open("secretpart"+str(i)+".txt", "ab") as f:
                        f.write(bytes([singlebyte[i]]))
        print("Divided Mode: Done")

    divide_mode = False
    if ~divide_mode:
        for j in range(0, os.path.getsize("secretpart1.txt")):
            parts = []
            for i in range(n):
                with open("secretpart"+str(i)+".txt", "rb") as f:
                    f.seek(j, 0)
                    byte = int.from_bytes(f.read(1), byteorder = sys.byteorder)
                    # byte = f.read(1)
                    parts.append( byte )
            s = 0
            for i in parts:
                s += i
            s %= k
            print(chr(s), end="")
        print("\nMerge Mode: Done")

