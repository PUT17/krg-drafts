# from sympy import nextprime
from random import randint, seed
# from numpy.random import choice
from numpy import polyval
from scipy.interpolate import lagrange
import itertools


def shamir_divide(s = 954, t = 3, n = 4, p = 1523):
    #
    # s - secret to divide, t - minimal parts count,
    # n - total number of parts, p - max range of secret (prime number)
    #

    # generating polynomial A
    a = []
    for i in range(t-1):
        # seed()
        a.append(randint(1,p))
    a.append(s)

    # calculating value of polynomial in i=1,2,...
    parts = list()
    for i in range(int(-n/2+1), int(n/2+1)):
    #for i in range(1, n+1):
        # generate single secret part
        part_value = polyval(a, i) % p
        parts.append(tuple((i, part_value)))

    # return list of tuples (i, polynomial_value)
    return parts

def shamir_merge(parts, p):
    #
    # parts - list of tuples (i, polynomial_value)
    # p - max range of secret
    #
    # length of parts[] must be greater than t
    #

    # vectors for calculating lagrange polynomial
    # x get values i from list of part tuples
    # y get values in points from that lists
    x = []
    y = []
    for part in parts:
        x.append(part[0])
        y.append(part[1])
    lagrange_polyval = lagrange(x,y)
    decoded_secret = int(lagrange_polyval.coeffs[-1]) % p
    return decoded_secret


def shamir_test(s = 954, t = 3, tused = 3, n = 4, p = 1523):
    parts = shamir_divide(s, t, n, p)

    choosenindexes_list = list(itertools.combinations(list(range(n)), tused))
    accuracy_counter = 0
    for indexset in choosenindexes_list:
        # print(indexset)
        randomparts = [parts[i] for i in indexset]
        merged_secret = shamir_merge(randomparts, p)
        if merged_secret == s:
            accuracy_counter += 1
        # else:
        #    print(merged_secret)
        try_number = len(choosenindexes_list)
    print(
         "Accuracy: " + "%.2f" % ((accuracy_counter / try_number) * 100) + "%\t(" + str(accuracy_counter) + "/" + str(
            try_number) + ")\ts=" + str(s) + "\tn=" + str(n) + "\tt=" + str(t) + "\tp=" + str(p))


if __name__ == "__main__":
    # shamir_test(s = 954, t = 3, tused = 3, n = 4, p = 1523):
    for i in range(10):
        shamir_test(1444, 3, 3, 4, 1523)
    # shamir_test(954, 4, 5, 6, 1523)
    # shamir_test(954, 4, 4, 4, 1523)
    # shamir_test(103, 3, 3, 4, 1523)
    # shamir_test(104, 3, 3, 4, 1523)
