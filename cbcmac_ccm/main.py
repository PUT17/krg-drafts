from ccm import CCM
from cbcmac import CBC_MAC
from cbcmac import AES_CBC_MAC_full_encrypt
import binascii
from datetime import datetime


def test_CBC_MAC(msg, ek1, ek2, iv=None):
    print("msg: " + str(binascii.hexlify(msg)))
    print("ek1: " + str(binascii.hexlify(ek1)))
    print("ek2: " + str(binascii.hexlify(ek2)))
    data = AES_CBC_MAC_full_encrypt(msg, ek1, ek2)
    # data = CBC_MAC(msg, ek1, ek2, iv)
    print("crp: " + str(binascii.hexlify(data)))


def test_CCM(msg, ek, iv=None, ctr=None):
    print("msg: " + str(binascii.hexlify(msg)))
    print("ek : " + str(binascii.hexlify(ek)))
    data = CCM(msg, ek, iv, ctr)
    print("crp: " + str(binascii.hexlify(data)))


if __name__ == "__main__":

    print("--------------------\nCMB_MAC:")
    a = datetime.now()
    test_CBC_MAC(b"Lorem ipsum dolor", b"0123456789aehikj", b"5563463189aehikj")
    b = datetime.now()
    c = b - a
    print(str(c.microseconds) + "[ms]")

    print("--------------------\nCCM:")
    a = datetime.now()
    test_CCM(b"Lorem ipsum dolor", b"0123456789aehikj")
    b = datetime.now()
    c = b - a
    print(str(c.microseconds) + "[ms]")