from cbcmac import CBC_MAC
from cbcmac import BLOCK_SIZE
from cbcmac import AES_CBC_encrypt
import binascii

def XORbytearray(a, b):
    if len(a) != len(b):
        raise Exception("XOR error. Different lengths " + str(len(a)) + "!=" + str(len(b)))
    for i in range(len(a)):
        b[i] ^= a[i]
    return b


def incrementCtr(ctr):
    tmp = int.from_bytes(ctr, byteorder="big")
    tmp += 1
    if tmp > 65535:
        tmp %= 65535
    ret = int.to_bytes(tmp, 16, byteorder="big")
    return ret


def CCM(message, ek, iv=None, counter=None):
    # set Init Vector for CBC MAC
    if iv == None:
        iv = b'4632466247945180'

    # get MAC from CBC
    MAC = CBC_MAC(message, ek, ek, iv)

    # init Counter Value
    if counter == None:
        counter = b'3452357897653217'

    # split message
    message_array = bytearray(message)  # contains all the message
    working_size = len(message)
    if len(message) % 16 != 0:
        working_size += (16-(len(message) % 16))    # round message length do %16
    # fill message with zeroes
    working_array = bytearray(working_size)
    for i in range(len(message_array)):
        working_array[i] = message_array[i]  # contains messeged filled with zeroses


    working_list = list()  # contains each bytearray for each AES loop
    for i in range(0, working_size, BLOCK_SIZE):
        working_list.append(working_array[i:i + BLOCK_SIZE])

    working_list.append(bytearray(MAC))

    # main algorithm
    ciphered = bytearray(0)
    for P in working_list:
        iv_btarry = bytearray(iv)
        ctr_btarry = bytearray(counter)
        msg_to_encrypt = XORbytearray(iv_btarry, ctr_btarry)
        x = AES_CBC_encrypt( bytes(msg_to_encrypt), ek)
        # counter += bytes(1)
        counter = incrementCtr(counter)
        ciphered += XORbytearray(bytearray(x), P)

    return ciphered







if __name__ == "__main__":
    print(binascii.hexlify(CCM(b"000", b"0000000000000000")))
    print(binascii.hexlify(CCM(b"0123", b"0123456789aehikj")))
    print(binascii.hexlify(CCM(b"0123456789aehikj", b"0123456789aehikj")))
    print(binascii.hexlify(CCM(b"0123456789aehikj0123456789aehikj0123456789aehikj0123456789aehikj", b"0123456789aehikj")))
    print("Done")
    # a = CBC_MAC(b"0123456789aehikj", b"0123456789aehikj", b"0123456789aehikj")
    # b = CBC_MAC(b"0123456789aehikj0123", b"0123456789aehikj", b"0123456789aehikj")
    # print(len(a))
    # print(len(b))
    # print("Done")