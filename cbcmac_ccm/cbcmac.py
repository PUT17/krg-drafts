from Crypto import Random
from Crypto.Cipher import AES


BLOCK_SIZE = 16 # 16 chars, 16 bytes


def AES_CBC_MAC_full_encrypt(message, ek1, ek2):
    message_array = bytearray(message)  # contains all the message
    working_size = len(message)
    if len(message) % 16 != 0:
        working_size += (16 - (len(message) % 16))
    working_array = bytearray(working_size)
    for i in range(len(message_array)):
        working_array[i] = message_array[i]  # contains messeged filled with zeroses

    working_list = list()  # contains each bytearray for each CBC MAC loop
    for i in range(0, working_size, BLOCK_SIZE):
        working_list.append(working_array[i:i + BLOCK_SIZE])

    encrypted = bytearray(0)
    for part in working_list:
        encrypted += AES_CBC_encrypt(bytes(part), ek1)
    encrypted += CBC_MAC(message, ek1, ek2)
    return bytes(encrypted)



def AES_CBC_encrypt(message, key, init_vector = None):
    if len(bytes(message)) > BLOCK_SIZE:
        raise Exception("Too many bytes in AES-CBC")
    if init_vector is None:
        init_vector = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, init_vector)
    return cipher.encrypt(message)


def CBC_MAC(message, ek1, ek2, iv=None):
    if iv == None:
        iv = b'0000000000000000';
    message_array = bytearray(message) # contains all the message
    working_size = len(message)
    if len(message) % 16 != 0:
        working_size += (16-(len(message) % 16))
    working_array = bytearray(working_size)
    for i in range(len(message_array)):
        working_array[i] = message_array[i]     # contains messeged filled with zeroses

    working_list = list()                       # contains each bytearray for each CBC MAC loop
    for i in range(0, working_size, BLOCK_SIZE):
        working_list.append(working_array[i:i+BLOCK_SIZE])

    ciphered = AES_CBC_encrypt(bytes(working_list[0]), ek1, iv)
    for i in range(1, len(working_list)-1):
        ciphered = AES_CBC_encrypt(bytes(working_list[i]), ek1, ciphered)
    ciphered = AES_CBC_encrypt(bytes(working_list[-1]), ek2, ciphered)

    return ciphered






if __name__ == "__main__":
    # print(AES_CBC_encrypt(b"abacacacacacacac", b"abacacacacacacac"))
    #for i in range(5):
    #    print(CBC_MAC(b"0123456789aefkipiu", b"0123456789aehikj", b"abacacacacacacac"))
    a = CBC_MAC(b"0123456789aehikj", b"0123456789aehikj", b"0123456789aehikj")
    b = CBC_MAC(b"0123456789aehikj0123", b"0123456789aehikj", b"0123456789aehikj")
    print(a)
    print(b)
    print(AES_CBC_MAC_full_encrypt(b"123123123132", b"0123456789aehikj", b"0123456789aehikj"))
    print("Done")



# msg = input('Message...: ')
# pwd = input('Password..: ')
# print('Ciphertext:', AESCipher(pwd).encrypt(msg))
#

